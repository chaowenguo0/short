import google.auth, google.auth.transport.requests, google.oauth2, asyncio, aiohttp, os, argparse, json, base64, urllib.parse

parser = argparse.ArgumentParser()
parser.add_argument('github')
credentials = google.oauth2.service_account.Credentials.from_service_account_file('gcloud', scopes=['https://www.googleapis.com/auth/cloud-platform'])
auth_req = google.auth.transport.requests.Request()
credentials.refresh(auth_req)

async def main():
    async with aiohttp.ClientSession() as session:
        job = 'https://cloudscheduler.googleapis.com/v1/projects/chaowenguo/locations/us-central1/jobs'
        repository = os.getenv('GITHUB_REPOSITORY').split('/')[-1]
        async with session.get('/'.join((job, repository)), headers={'authorization':f'Bearer {credentials.token}'}) as response:
            if response.status == 200:
                async with session.delete('/'.join((job, repository)), headers={'authorization':f'Bearer {credentials.token}'}) as _: pass
        async with session.post(job, headers={'authorization':f'Bearer {credentials.token}'}, json={'name':'/'.join((urllib.parse.urlparse(job).path.split('/', 2)[-1], repository)), 'schedule':'*/10 * * * *', 'httpTarget':{'uri':f'https://api.github.com/repos/{os.getenv("GITHUB_REPOSITORY")}/dispatches', 'headers':{'Authorization':f'token {parser.parse_args().github}'}, 'body':base64.b64encode(json.dumps({'event_type':repository}).encode()).decode()}}) as _: pass
        
asyncio.run(main())
#if [[ `gcloud scheduler jobs describe ${GITHUB_REPOSITORY#*/} 2>&1` != *ERROR* ]]
#then
#    gcloud scheduler jobs delete ${GITHUB_REPOSITORY#*/} -q
#fi
#gcloud scheduler jobs create http ${GITHUB_REPOSITORY#*/} --schedule '0 */3 * * *' --uri https://api.github.com/repos/$GITHUB_REPOSITORY/dispatches --headers Authorization='token ${{secrets.GITHUB}}' --message-body '{"event_type":"'${GITHUB_REPOSITORY#*/}'"}'
#gcloud scheduler jobs run ${GITHUB_REPOSITORY#*/}
